@extends('layaout usuario.principal')

@section('titulo')
    <title>HISTORIAL</title>
@endsection

@section('css')
    <link rel="stylesheet" href="css/peliculas.css">
    <title> Inicio </title>
@endsection
@section('contenido')
    @php $pelicula = \App\Models\Pelicula::get();    @endphp
    <div class="container">
        <h1 class="title">Historial</h1>
        <div class="row">
            <div class="col-12">
                <table class="table table-image">
                    <thead>
                    <tr>
                        <th scope="col">Miniatura</th>
                        <th scope="col">Pelicula</th>
                        <th scope="col">Min. Reproducidos</th>
                        <th scope="col">Min. Totales</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($pelicula as $uso)
                    <tr>
                        <td class="w-25">
                            <img src="{{$uso->ruta_imagen1}}" class="img-fluid img-thumbnail" alt="Sheep">
                        </td>
                        <td>{{$uso->nombre}}</td>
                        <td>123.2</td>
                        <td>{{$uso->tiempo_duracion}}</td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection

@section('js')
    <script src="../recursos/js/jquery.min.js"></script>
    <script src="../recursos/js/bootstrap.js"></script>
@endsection
