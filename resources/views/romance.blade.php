@extends('layaout usuario.principal')

@section('titulo')
    <title>ROMANCE</title>
@endsection

@section('css')
    <link rel="stylesheet" href="css/peliculas.css">
    <title> Inicio </title>
@endsection
@section('contenido')
<div class="container">
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <h3 class=" pb-4 mb-4 pt-4 font-italic border-top border-bottom">
                Las mejores peliculas de Romance:
                </h3>
                <div class="input-group pb-4">
                    <input type="text" class="form-control mr-sm-2"type="search" placeholder="Ingrese pelicula">
                    <div class="input-group-append">
                        <button class="btn btn-danger">Buscar</button>
                    </div>
                </div>
        @php $pelicula = \App\Models\Pelicula::where('categoria','Romance')->get(); @endphp
            @foreach($pelicula as $uso)
                <div class="row d-flex align-items-stretch">
                    <div class="col-12 col-sm-6 col-md-3 m-1 pelicula">
                        <div class="card">
                            <div class="card-body p-0">
                            <h5 class="ml-5">{{$uso->nombre}}</h5>
                            <img src="{{$uso->ruta_imagen1}}" class="img-fluid">
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach                   
                
            </div>
            <aside class="col-md-4 pb-4">
                
            </aside>
        </div>
    </div>
    <div class="container border-bottom border-top rounded">
        <div class="card pie">
            <div class="card-body text-light">
                <strong>Mas informacion en:
                    <a class="text-light"href="https://www.facebook.com/">El manco</a>
                </strong>
                <div class="text-light float-right">
                    <b>Manco</b>2.0
                </div>
            </div>
        </div>
    </div>
    <script src="../recursos/js/jquery.min.js"></script>
    <script src="../recursos/js/bootstrap.js"></script>
@endsection

@section('js')

@endsection