<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    @yield('titulo')
    <title>UPTFLIX | UPTFLIX</title>
    <!-- Favicon-->
    <link rel="icon" type="image/x-icon" href="assets/favicon.ico" />
    <!-- Bootstrap icons-->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css" rel="stylesheet" />
    <!-- Core theme CSS (includes Bootstrap)-->
    <link href="css/principal.css" rel="stylesheet" />

    @yield('css')
</head>
<body>
<!-- Navigation-->
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container px-4 px-lg-5">
        <nav class="div-navbar-nav">
            <i class="div-navbar-nav-logo"><img src="https://dewey.tailorbrands.com/production/brand_version_mockup_image/938/5552771938_b0ed4e40-4af4-4d93-b5e5-278f289700c3.png?cb=1624927903" width="170px" height="45px"></i>
        </nav>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0 ms-lg-4">
                <li class="nav-item"><a class="nav-link active" aria-current="page" href="{{route('vista.usuario.terror')}}">Terror</a></li>
                <li class="nav-item"><a class="nav-link active" aria-current="page" href="{{route('vista.usuario.comedia')}}">Comedia</a></li>
                <li class="nav-item"><a class="nav-link active" aria-current="page" href="{{route('vista.usuario.accion')}}">Accion</a></li>
                <li class="nav-item"><a class="nav-link active" aria-current="page" href="{{route('vista.usuario.drama')}}">Drama</a></li>
                <li class="nav-item"><a class="nav-link active" aria-current="page" href="{{route('vista.usuario.romance')}}">Romance</a></li>
                <li class="nav-item"><a class="nav-link active" aria-current="page" href="{{route('vista.usuario.animadas')}}">Animadas</a></li>
                <li class="nav-item"><a class="nav-link active" aria-current="page" href="{{route('vista.usuario.ciencia_ficcion')}}">Ciencia Ficcion</a></li>
                <li class="nav-item"><a class="nav-link active" aria-current="page" href="{{route('vista.usuario.musicales')}}">Musicales</a></li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle bi bi-person-circle" id="navbarDropdown" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false"></a>
                    <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <li><a class="dropdown-item" href="#!">Mi perfil</a></li>
                        <li><a class="dropdown-item" href="#!">Perfiles</a></li>
                        <li><a class="dropdown-item" href="{{route('vista.usuario.historial')}}">Historial</a></li>
                        <li><a class="dropdown-item" href="#!">Cerrrar Sesion</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>

<!-- Section-->
<section class="py-5">
    <div class="container px-4 px-lg-5 mt-5">
        <div class="row gx-4 gx-lg-5 row-cols-2 row-cols-md-3 row-cols-xl-4 justify-content-center">

        </div>
        @yield('contenido')
    </div>
</section>
<!-- Footer-->
<footer class="py-5 bg-dark">
    <div class="container"><p class="m-0 text-center text-white">Copyright &copy; Los mancos 2021</p></div>
</footer>
<!-- Bootstrap core JS-->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js"></script>
<!-- Core theme JS-->
<script src="js/scripts.js"></script>

@yield('js')
</body>
</html>
