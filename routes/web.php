<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UsuarioController;
use App\Http\Controllers\PeliculaController;
use App\Http\Controllers\PaqueteController;


Route::get('/', function () {
    return view('inicio');
});

Route::get('/principal',[UsuarioController::class,'principal'])->name('principal');
Route::get('/login',[UsuarioController::class,'login'])->name('login');
Route::post('/login',[UsuarioController::class,'verificaCredencialesLogin'])->name('verifica.credenciales.login');


Route::post('/registroUsuario',[UsuarioController::class,'verificaCorreo'])->name('verifica.correo'); //escribe el correo ya en el formulario
Route::get('/registro',[UsuarioController::class,'registro'])->name('registro.usuario'); //obtiene la vista de registro sin el correo en el formulario
Route::post('/registro',[UsuarioController::class,'registroSlider'])->name('registro.form'); //envia el formulario

//Vista principal y de categorias
Route::post('/principal',[PeliculaController::class,'principal'])->name('principal');
//Retordo de las vistas de el layaout
Route::get('/terror',[PeliculaController::class,'terror'])->name('vista.usuario.terror');
Route::get('/comedia',[PeliculaController::class,'comedia'])->name('vista.usuario.comedia');
Route::get('/accion',[PeliculaController::class,'accion'])->name('vista.usuario.accion');
Route::get('/drama',[PeliculaController::class,'drama'])->name('vista.usuario.drama');
Route::get('/romance',[PeliculaController::class,'romance'])->name('vista.usuario.romance');
Route::get('/animadas',[PeliculaController::class,'animadas'])->name('vista.usuario.animadas');
Route::get('/cienciaFiccion',[PeliculaController::class,'ciencia_ficcion'])->name('vista.usuario.ciencia_ficcion');
Route::get('/musicales',[PeliculaController::class,'musicales'])->name('vista.usuario.musicales');
Route::get('/historial',[PeliculaController::class,'historial'])->name('vista.usuario.historial');

//vista player
Route::get('/peliculaRe',[PeliculaController::class,'videoPlayer'])->name('vista.usuario.video');


//ruta provisional agregar peliculas admin.
Route::get('/addPeliculasAdmin',[PeliculaController::class,'addPeliculas'])->name('addPeliculas');
Route::post('/addPeliculasAdmin',[PeliculaController::class,'registrarPelicula'])->name('add.peliculas');
//ruta provisional agregar paquetes admin.
Route::get('/addPaquetesAdmin',[PaqueteController::class,'addPaquetes'])->name('addPaquetes');
Route::post('/addPaquetesAdmin',[PaqueteController::class,'registrarPaquete'])->name('add.paquete');

//ruta provisional peliculas administrador.
Route::get('/peliculasAdmin',[PeliculaController::class,'peliculasAdmin'])->name('peliculas.admin');
//ruta provisional paquetes administrador.
Route::get('/paquetesAdmin',[PaqueteController::class,'paquetesAdmin'])->name('paquetes.admin');

//ruta provisional actualizar peliculas administrador.
Route::get('/actualizar/{id_pelicula}/',[PeliculaController::class,'vistaActualizarPeliculasAdmin'])->name('actualizar.peliculas.admin');
Route::post('/actualizarPelicula',[PeliculaController::class,'actualizarPelicula'])->name('act.peliculas.admin.post');

// //ruta provisional eliminar peliculas administrador. vista provisional ventana emergente.
Route::post('/eliminarPelicula',[PeliculaController::class,'ventanaEmergente'])->name('ventana.emergente');
// //ruta provisional eliminar paquetes administrador. vista provisional ventana emergente.
Route::post('/elminarPaquete',[PaqueteController::class,'ventanaEmergentePaquetes'])->name('ventana.emergente.paquetes');

//ruta provisional actualizar paquetes administrador.
Route::get('/actualizarPaquete/{id_paquete}/',[PaqueteController::class,'vistaActualizarPaqueteAdmin'])->name('actualizar.paquete.admin');
Route::post('/actualizarPaquete',[PaqueteController::class,'actualizarPaquete'])->name('act.paquete.admin.post');

