<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Iniciar Sesión</title>
    <!-- Estilos custom -->
    <link rel="stylesheet" href="css/estilosLogin.css">
    <!-- Fuentes -->
    <link href="https://fonts.googleapis.com/css2?family=Raleway:wght@100&display=swap" rel="stylesheet">
    <!-- Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body>
<header>
    <nav class="div-navbar-nav">
        <i class="div-navbar-nav-logo"><img src="https://dewey.tailorbrands.com/production/brand_version_mockup_image/323/5553860323_8d2c7ac2-3b20-4444-a2b2-8a8df9f2525c.png?cb=1624942953" width="170px" height="45px"></i>
    </nav>
</header>
    <div class="contenedor">
        <form action="{{route('verifica.credenciales.login')}}" method="post">
            {{csrf_field()}}
            <h1 class="contenedor__titulo">Login</h1>
            @if(isset($estatus))
                @if($estatus == "error")
                    <label class="label-error">{{$mensaje}}</label>
                @endif
            @endif
            <input type="text"  placeholder="Correo" name="correo">
            <input type="password" placeholder="Contraseña" name="contrasenia">
            <input type="submit" value="Ingresar">
            <h4>¿No tienes cuenta? <a href="/"><b>¡Crea una!</b></a></h4>
        </form>
    </div>
</body>
</html>
