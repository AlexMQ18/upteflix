<?php

namespace App\Http\Controllers;

use App\Models\Pelicula;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;


class PeliculaController extends Controller
{

    //funcion provisional que retorna vista agregar peliculas.
    public function addPeliculas()
    {
        return view("addPeliculasAdmin");
    }
    //funcion provisional que retorna vista peliculas administrador.
    public function peliculasAdmin()
    {
        return view("peliculasAdmin");
    }
    //Retorna las vistas de el layaout
    public function terror(){
        return view("terror");
    }
    public function comedia(){
        return view("comedia");
    }
    public function accion(){
        return view("accion");
    }
    public function drama(){
        return view("drama");
    }
    public function romance(){
        return view("romance");
    }
    public function animadas(){
        return view("animadas");
    }
    public function ciencia_ficcion(){
        return view("ciencia_ficcion");
    }
    public function musicales(){
        return view("musicales");
    }
    public function historial(){
        return view("historialUsuario");
    }
    public function videoPlayer(){
        return view("videoPlayer");
    }


    //CRUD VISTA ADMINISTRADOR.

    //REGISTRAR PELICULA
    public function registrarPelicula(Request $datos)
    {
        //FUNCION QUE QUE REGISTRARA LOS DATOS DE LA PELICULA EN LA BD
        if (!$datos->nombre || !$datos->categoria || !$datos->minutos
            || !$datos->descripcion || !$datos->anio) {
            //VALIDACION DE LOS DATOS ANTES DE SUBIRSE
            return view("addPeliculasAdmin", ["estatus" => "error", "mensaje" => "¡Falta información!"]);
        } else {
            //Subir 2 imagenes a la bd.
            $datos->validate([
                'img1' => 'required|image|max:4000' //portada
            ]);
            $datos->validate([
                'img2' => 'required|image|max:4000' //imagen secundaria
            ]);

            $img1 = $datos->file('img1')->store('public/peliculas'); //subida a la bd
            $img2 = $datos->file('img2')->store('public/peliculas');
            $url1 = Storage::url($img1);
            $url2 = Storage::url($img2);


            $pelicula = new Pelicula();     //Validaciones correctas, subida a bd.
            $pelicula->nombre = $datos->nombre;
            $pelicula->categoria = $datos->categoria;
            $pelicula->tiempo_duracion = $datos->minutos;
            $pelicula->descripcion = $datos->descripcion;
            $pelicula->ruta_imagen1 = $url1;
            $pelicula->ruta_imagen2 = $url2;
            $pelicula->anio = $datos->anio;
            $pelicula->save(); //datos guardados.

            return view("addPeliculasAdmin",
                ["estatus" => "success", "mensaje" => "¡Pelicula agregada exitosamente!"]);
        }
    }

    //ELIMINAR PELICULA
    public function ventanaEmergente(Request $datos)
    {
        $query = Pelicula::find($datos->id_pelicula);
        if ($query){
            $query->delete();
            return response()->json(['mensaje'=>'success']);
        }else{
            return response()->json(['mensaje'=>'error']);
        }
    }

    //VISTA ACTUALIZAR PELICULAS
    public function vistaActualizarPeliculasAdmin($id_pelicula)
    {
        $consulta = Pelicula::where('id_pelicula', $id_pelicula)->get()->first();
        return view("actualizarPeliculasAdmin", ["datos" => $consulta]);
    }

    //ACTUALIZAR PELICULA
    public function actualizarPelicula(Request $datos)
    {
    //actualizacion de datos.
    $id_pelicula = $datos->id_pelicula;
        //Subir 2 imagenes a la bd.
        $datos->validate([
            'img1' => 'required|image|max:4000', //portada
            'img2' => 'required|image|max:4000', //SEGUNDA portada
            'nombre' => 'required',
            'categoria' => 'required',
            'minutos' => 'required',
            'descripcion' => 'required'
        ]);
        $datos->validate([
             //imagen secundaria
        ]);

        $img1 = $datos->file('img1')->store('public/peliculas'); //subida a la bd
        $img2 = $datos->file('img2')->store('public/peliculas');
        $url1 = Storage::url($img1);
        $url2 = Storage::url($img2);


        $pelicula = Pelicula::where('id_pelicula', $id_pelicula)->get()->first();   //Validaciones correctas, subida a bd.
        $pelicula->nombre = $datos->nombre;
        $pelicula->categoria = $datos->categoria;
        $pelicula->tiempo_duracion = $datos->minutos;
        $pelicula->descripcion = $datos->descripcion;
        $pelicula->ruta_imagen1 = $url1;
        $pelicula->ruta_imagen2 = $url2;
        $pelicula->anio = $datos->anio;
        $pelicula->save(); //datos guardados.

        return view("peliculasAdmin",
            ["estatus" => "success", "mensaje" => "¡Pelicula actualizada exitosamente!"]);
    }



}
