<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Paquete;
use Illuminate\Support\Facades\Storage;

class PaqueteController extends Controller
{
    //funcion provisional que retorna vista agregar paquetes.
    public function addPaquetes()
    {
        return view("addPaquetesAdmin");
    }
    //funcion provisional que retorna vista de los paquetes agregados.
    public function paquetesAdmin()
    {
        return view("paquetesAdmin");
    }
    //REGISTRAR PAQUETE
    public function registrarPaquete(Request $datos)
    {
        if (!$datos->nombrePaquete || !$datos->precio
            || !$datos->descripcion) {
            //VALIDACION DE LOS DATOS ANTES DE SUBIRSE
            return view("addPaquetesAdmin", ["estatus" => "error", "mensaje" => "¡Falta información!"]);
        } else {

            $paquete = new Paquete();
            $paquete->nombre_paquete = $datos->nombrePaquete;
            $paquete->precio = $datos->precio;
            $paquete->descripcion = $datos->descripcion;
            $paquete->resolucion = $datos->resolucion;
            $paquete->save();

            return view("addPaquetesAdmin",
                ["estatus" => "success", "mensaje" => "¡Paquete agregado exitosamente!"]);

        }
    }

    //ELIMINAR PAQUETE
    public function ventanaEmergentePaquetes(Request $datos)
    {
        $query = Paquete::find($datos->id_paquete);
        if ($query){
            $query->delete();
            return response()->json(['mensaje'=>'success']);
        }else{
            return response()->json(['mensaje'=>'error']);
        }
    }

    //VISTA ACTUALIZAR PAQUETE
    public function vistaActualizarPaqueteAdmin($id_paquete)
    {
        $consulta = Paquete::where('id_paquete', $id_paquete)->get()->first();
        return view("actualizarPaqueteAdmin", ["datos" => $consulta]);
    }
    //ACTUALIZAR PAQUETE
    public function actualizarPaquete(Request $datos)
    {
        $id_paquete = $datos->id_paquete;
        $datos->validate([
            'nombrePaquete' => 'required',
            'precio' => 'required',
            'descripcion' => 'required'
        ]);

        $paquete = Paquete::where('id_paquete', $id_paquete)->get()->first();
        $paquete->nombre_paquete = $datos->nombrePaquete;
        $paquete->precio = $datos->precio;
        $paquete->descripcion = $datos->descripcion;
        $paquete->resolucion = $datos->resolucion;
        $paquete->save();

        return view("paquetesAdmin",
            ["estatus" => "success", "mensaje" => "¡Paquete actualizado exitosamente!"]);


    }


}
