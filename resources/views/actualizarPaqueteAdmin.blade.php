@extends('layout_admin.admin')

@section('titulo')
    <title>VISTA ADMIN | Paquetes</title>
@endsection

@section('css')
    <!-- Estilos custom -->
    <link rel="stylesheet" href="../css/crudPeliculas_beta.css">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <!-- Fuentes -->
    <link href="https://fonts.googleapis.com/css2?family=Raleway:wght@100&display=swap" rel="stylesheet">
    <link href="../css/estilosAdmin.css" rel="stylesheet" />
@endsection

@section('contenido')
    <div class="contenedor">
        <form class="contenedor__form"  action="{{route('act.paquete.admin.post')}}" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            <h1 class="form__h1">Agregar un paquete.</h1>
            <input type="hidden" name="id_paquete" value="{{$datos->id_paquete}}">
            <label>Ingresa el nombre del paquete.</label>
            @error('nombrePaquete')
            <label class="label-error">{{$message}}</label>
            @enderror
            <input class="contenedor__input" type="text" name="nombrePaquete" value="{{$datos->nombre_paquete}}">
            <label>Ingresa el precio del paquete</label>
            @error('precio')
            <label class="label-error">{{$message}}</label>
            @enderror
            <div class="contenedor__number">
                <label>$ </label>
                <input class="contenedor__input" type="number" min="0"  max="1000" name="precio" value="{{$datos->precio}}">
            </div>
            <label>Ingresar descripcion del paquete</label>
            @error('descripcion')
            <label class="label-error">{{$message}}</label>
            @enderror
            <textarea class="contenedor__textarea" id="" cols="30" rows="10" name="descripcion">{{$datos->descripcion}}</textarea>
            <label>Ingresa la resolucion del paquete.</label>
            <select class="contenedor__select" name="resolucion">
                <option value="720" @if($datos->resolucion == "720") selected @endif>720p</option>
                <option value="1080" @if($datos->resolucion == "1080") selected @endif>1080p</option>
                <option value="1440" @if($datos->resolucion == "1440") selected @endif>1440p</option>
            </select>
            @if(isset($estatus))
                @if($estatus == "error")
                    <label class="label-error">{{$mensaje}}</label>
                @endif
                @if($estatus == "success")
                    <label class="label-success">{{$mensaje}}</label>
                @endif
            @endif
            <input class="contenedor__submit" type="submit">
        </form>
    </div>
@endsection

@section('js')
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
    <script src="../js/scripts.js"></script>
@endsection
