-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 20-07-2021 a las 20:48:06
-- Versión del servidor: 10.4.14-MariaDB
-- Versión de PHP: 7.4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `uptflix_bd`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `historial_usuario`
--

CREATE TABLE `historial_usuario` (
  `id_historial` int(11) NOT NULL,
  `id_pelicula` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `minuto_reproduccion` varchar(80) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `paquete`
--

CREATE TABLE `paquete` (
  `id_paquete` int(11) NOT NULL,
  `nombre_paquete` varchar(100) NOT NULL,
  `precio` varchar(100) NOT NULL,
  `descripcion` varchar(300) NOT NULL,
  `resolucion` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `paquete`
--

INSERT INTO `paquete` (`id_paquete`, `nombre_paquete`, `precio`, `descripcion`, `resolucion`, `created_at`, `updated_at`) VALUES
(4, '¡Ahorra datos!', '100', 'El paquete ideal para salir de viaje y ahorrar tu consumo. Con el podrás seguir con el entretenimiento sin gastar demasiado.', '720', '2021-07-17 18:07:13', '2021-07-17 23:07:13'),
(5, 'Alta definición', '250', 'El paquete ideal para ver tus películas favoritas en pantallas de alta resolución en ¡1080p!', '1080', '2021-07-17 23:16:31', '2021-07-17 23:16:31'),
(6, 'QHD', '300', 'Este paquete es para los amantes de la alta resolución ya que con el podrás ver tus peliculas en 2K', '1440', '2021-07-17 23:22:10', '2021-07-17 23:22:10');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pelicula`
--

CREATE TABLE `pelicula` (
  `id_pelicula` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `categoria` varchar(150) NOT NULL,
  `tiempo_duracion` varchar(50) NOT NULL,
  `descripcion` text NOT NULL,
  `ruta_imagen1` varchar(200) NOT NULL,
  `ruta_imagen2` varchar(200) NOT NULL,
  `anio` varchar(50) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `pelicula`
--

INSERT INTO `pelicula` (`id_pelicula`, `nombre`, `categoria`, `tiempo_duracion`, `descripcion`, `ruta_imagen1`, `ruta_imagen2`, `anio`, `created_at`, `updated_at`) VALUES
(17, 'Geminis 2', 'Accion', '110', 'SEDFG SDF', '/storage/peliculas/ilnWXKXliiHW1yIjvuxDUV7C4nMluehwLT1cBoLJ.png', '/storage/peliculas/lq1NRFn6p15kmQAoccpWl3wL3OtDXXlgAsDIgZ5w.jpg', '2018-07-12', '2021-07-15 20:09:01', '2021-07-16 01:09:01'),
(21, 'asdasd', 'Accion', '123', 'fasfasf', '/storage/peliculas/NlqPcctafRY5C08FudHgIxI39oRJ2kWVlztXEjHY.png', '/storage/peliculas/PAWNwqS5FeqAi9dxoJ2sOY89FKb4ZCESft7RfaHW.png', '2018-07-17', '2021-07-06 06:43:18', '2021-07-06 06:43:18');

--
-- Disparadores `pelicula`
--
DELIMITER $$
CREATE TRIGGER `actualizar_peliculas_trigger` BEFORE UPDATE ON `pelicula` FOR EACH ROW INSERT INTO `peliculas_actualizaciones` SET `id_pelicula` = old.`id_pelicula`,`nombre` = old.`nombre`,`categoria` = old.`categoria`,`tiempo_duracion` = old.`tiempo_duracion`,`descripcion` = old.`descripcion`,`ruta_imagen1` = old.`ruta_imagen1`,`ruta_imagen2` = old.`ruta_imagen2`,`anio` = old.`anio`, `fecha_cambio` = now(), action = 'update'
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `peliculas_eliminadas_trigger` BEFORE DELETE ON `pelicula` FOR EACH ROW INSERT INTO `peliculas_eliminadas`(`id_pelicula`, `nombre`, `categoria`, `tiempo_duracion`, `descripcion`, `ruta_imagen1`, `ruta_imagen2`, `anio`, `fecha_cambio`, `action`) VALUES (old.`id_pelicula`,old.`nombre`,old.`categoria`,old.`tiempo_duracion`,old.`descripcion`,old.`ruta_imagen1`,old.`ruta_imagen2`,old.`anio`,now(),`action` = 'delete')
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `peliculas_actualizaciones`
--

CREATE TABLE `peliculas_actualizaciones` (
  `id_cambio` int(11) NOT NULL,
  `id_pelicula` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `categoria` varchar(150) NOT NULL,
  `tiempo_duracion` varchar(50) NOT NULL,
  `descripcion` text NOT NULL,
  `ruta_imagen1` varchar(200) NOT NULL,
  `ruta_imagen2` varchar(200) NOT NULL,
  `anio` varchar(50) NOT NULL,
  `fecha_cambio` datetime NOT NULL,
  `action` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `peliculas_actualizaciones`
--

INSERT INTO `peliculas_actualizaciones` (`id_cambio`, `id_pelicula`, `nombre`, `categoria`, `tiempo_duracion`, `descripcion`, `ruta_imagen1`, `ruta_imagen2`, `anio`, `fecha_cambio`, `action`) VALUES
(1, 18, 'GODZILLA', 'Acción', '121', 'sdf afs as', '/storage/peliculas/P005kFS6HYfYMxk3zu2wqVUiiYXHzWtzaEaZmvdz.jpg', '/storage/peliculas/sDIucraBbOuT7BQfMMCEQAsFyRKvKUl4EhCEpSCs.jpg', '2018-07-18', '2021-06-27 16:59:17', 'update'),
(2, 17, 'Geminis', 'Acción', '110', 'SEDFG SDF', '/storage/peliculas/efMCkln1zmEoDwp17VnnLAeBmLQcuXz2VecLN0FN.jpg', '/storage/peliculas/KvjbslSAGPqeJay30QpWsEhF2yFstjOmksorH6hi.jpg', '2018-07-12', '2021-07-05 20:33:09', 'update'),
(3, 17, 'Geminis', 'Accion', '110', 'SEDFG SDF', '/storage/peliculas/efMCkln1zmEoDwp17VnnLAeBmLQcuXz2VecLN0FN.jpg', '/storage/peliculas/KvjbslSAGPqeJay30QpWsEhF2yFstjOmksorH6hi.jpg', '2018-07-12', '2021-07-15 15:09:01', 'update');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `peliculas_eliminadas`
--

CREATE TABLE `peliculas_eliminadas` (
  `id_cambio` int(11) NOT NULL,
  `id_pelicula` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `categoria` varchar(150) NOT NULL,
  `tiempo_duracion` varchar(50) NOT NULL,
  `descripcion` text NOT NULL,
  `ruta_imagen1` varchar(200) NOT NULL,
  `ruta_imagen2` varchar(200) NOT NULL,
  `anio` varchar(50) NOT NULL,
  `fecha_cambio` datetime NOT NULL,
  `action` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `peliculas_eliminadas`
--

INSERT INTO `peliculas_eliminadas` (`id_cambio`, `id_pelicula`, `nombre`, `categoria`, `tiempo_duracion`, `descripcion`, `ruta_imagen1`, `ruta_imagen2`, `anio`, `fecha_cambio`, `action`) VALUES
(2, 16, 'Star Wars', 'Ciencia Ficción', '212', 'fsdfdsf sdfsd', '/storage/peliculas/MHgvgRZkmqW2OS4vjeuxb1q7033waNd7pgiO4p67.jpg', '/storage/peliculas/EXPREHq0Pq96tAnEu8xTubN8Ca7I4kOLyXondsQf.jpg', '2018-07-10', '2021-06-27 19:07:21', '0'),
(3, 19, 'Aladin', 'Familiar', '154', 'dsjssdv sdvsv', '/storage/peliculas/mBxmhkZZ6WeMynJSLfBbR1zBpcZR7wCwKVICSqTI.jpg', '/storage/peliculas/B3RLteCMrHcjs5yD1Z1jyCgSoxMn34UkltaURHo0.jpg', '2018-07-22', '2021-06-30 12:26:30', '0'),
(4, 20, 'ada', 'asds', 'erresdf', 'sdfsdf', '/storage/peliculas/LY3CAv4D4qrKJ3PQbIgH7CKm5v1qPaJ52xaR9SLQ.png', '/storage/peliculas/wgv3cl0GVaeppgQr7Gqb8EIzLKTsQCnSMuHcfxgG.png', '2018-07-25', '2021-07-05 20:45:40', '0'),
(5, 22, 'nueva', '12333', '12', 'asfasfas', '/storage/peliculas/rLDiBZz4WSiHzwzcmInTgN0j77Ha0UII7TPc4s4y.png', '/storage/peliculas/XpPSzKwhyQwD4tCI3xoXl8tRBG4eAgcc3HJN2wap.png', '2018-07-22', '2021-07-15 12:54:11', '0');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tarjeta`
--

CREATE TABLE `tarjeta` (
  `id_tarjeta` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `apellido_parterno` varchar(50) NOT NULL,
  `apellido_materno` varchar(50) NOT NULL,
  `num_tarjeta` varchar(20) NOT NULL,
  `cvv` int(5) NOT NULL,
  `mes` varchar(15) NOT NULL,
  `anio` int(5) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `id_usuario` int(11) NOT NULL,
  `nombre_perfil` varchar(100) NOT NULL,
  `correo` varchar(100) NOT NULL,
  `contrasenia` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`id_usuario`, `nombre_perfil`, `correo`, `contrasenia`, `created_at`, `updated_at`) VALUES
(1, '', 'isai@correo.com', '123', '2021-06-12 23:30:27', '2021-06-12 23:30:27');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario_paquete`
--

CREATE TABLE `usuario_paquete` (
  `id_usuario_paquete` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `id_paquete` int(11) NOT NULL,
  `fecha_contrato` date NOT NULL,
  `fecha_vencimiento` date NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario_tarjeta`
--

CREATE TABLE `usuario_tarjeta` (
  `id_usuario_tarjeta` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `id_tarjeta` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `historial_usuario`
--
ALTER TABLE `historial_usuario`
  ADD PRIMARY KEY (`id_historial`);

--
-- Indices de la tabla `paquete`
--
ALTER TABLE `paquete`
  ADD PRIMARY KEY (`id_paquete`);

--
-- Indices de la tabla `pelicula`
--
ALTER TABLE `pelicula`
  ADD PRIMARY KEY (`id_pelicula`);

--
-- Indices de la tabla `peliculas_actualizaciones`
--
ALTER TABLE `peliculas_actualizaciones`
  ADD PRIMARY KEY (`id_cambio`);

--
-- Indices de la tabla `peliculas_eliminadas`
--
ALTER TABLE `peliculas_eliminadas`
  ADD PRIMARY KEY (`id_cambio`);

--
-- Indices de la tabla `tarjeta`
--
ALTER TABLE `tarjeta`
  ADD PRIMARY KEY (`id_tarjeta`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id_usuario`);

--
-- Indices de la tabla `usuario_paquete`
--
ALTER TABLE `usuario_paquete`
  ADD PRIMARY KEY (`id_usuario_paquete`);

--
-- Indices de la tabla `usuario_tarjeta`
--
ALTER TABLE `usuario_tarjeta`
  ADD PRIMARY KEY (`id_usuario_tarjeta`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `historial_usuario`
--
ALTER TABLE `historial_usuario`
  MODIFY `id_historial` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `paquete`
--
ALTER TABLE `paquete`
  MODIFY `id_paquete` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `pelicula`
--
ALTER TABLE `pelicula`
  MODIFY `id_pelicula` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT de la tabla `peliculas_actualizaciones`
--
ALTER TABLE `peliculas_actualizaciones`
  MODIFY `id_cambio` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `peliculas_eliminadas`
--
ALTER TABLE `peliculas_eliminadas`
  MODIFY `id_cambio` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `tarjeta`
--
ALTER TABLE `tarjeta`
  MODIFY `id_tarjeta` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id_usuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `usuario_paquete`
--
ALTER TABLE `usuario_paquete`
  MODIFY `id_usuario_paquete` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `usuario_tarjeta`
--
ALTER TABLE `usuario_tarjeta`
  MODIFY `id_usuario_tarjeta` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
