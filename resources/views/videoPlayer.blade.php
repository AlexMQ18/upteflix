@extends('layaout usuario.principal')

@section('titulo')
    <title>ACCION</title>
@endsection

@section('css')
    <link rel="stylesheet" href="css/estilosReproductor.css">
    <title> Inicio </title>
@endsection
@section('contenido')
    <div class="container">
        <h1 class="container__titulo-pelicula">Titulo pelicula</h1>
        <div class="caja-video">
            <span>Reproduciendo</span>
        </div>
        <div class="progress">
            <div class="progress-bar bg-danger bar-custom"
                 role="progressbar"
                 aria-valuemin="0"
                 aria-valuemax="100"
                 style="width: 50%"></div>
        </div>
        <h1 class="container__descripcion-titulo">Descripcion</h1>
        <p class="container__descripcion">
            Lorem ipsum dolor sit amet,
            consectetur adipisicing elit.
            Adipisci asperiores eaque,
            est officia reprehenderit sit tempora unde.
            Ab corporis eaque earum facilis modi odio qui saepe,
            similique tempore ullam, unde.</p>

    </div>
@endsection

@section('js')
    <script src="js/jquery-3.6.0.js"></script>
    <script>
        $(document).ready(function (){
           console.log("documento listo")
        });
    </script>
@endsection
