<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>UPTFLIX | Inicio</title>
    <!-- Estilos custom -->
    <link rel="stylesheet" href="css/estilosRaiz.css">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <!-- Fuentes -->
    <link href="https://fonts.googleapis.com/css2?family=Raleway:wght@100&display=swap" rel="stylesheet">
    <!-- Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body>
    <header>
        <nav class="div-navbar-nav">
            <i class="div-navbar-nav-logo"><img src="https://dewey.tailorbrands.com/production/brand_version_mockup_image/938/5552771938_b0ed4e40-4af4-4d93-b5e5-278f289700c3.png?cb=1624927903" width="170px" height="45px"></i>
            <a href="{{route('login')}}"><b>Iniciar Sesión</b></a>
        </nav>
    </header>
    <div class="contenedor">
        <h1 class="contenedor-titulo">¡UPTFLIX! La mejor plataforma para ver tus peliculas o series favoritas.</h1>
        <div class="contenedor-correo">
            <p>Registrate ahora mismo con tu correo: </p>
            @if(isset($estatus))
                @if($estatus == "error")
                    <label class="label-error">{{$mensaje}}</label>
                @endif
            @endif
            <form action="{{route('verifica.correo')}}" method="post"  id="form">
                {{csrf_field()}}
                <input type="text" name="input_correo" id="input_correo" placeholder="correo@ejemplo.com" required>
                <button type="submit">Iniciar</button>
            </form>
        </div>
    </div>

    <script src="js/jquery-3.6.0.js"></script>
    <script>
        $(document).ready(function (){
            console.log("document ready");
           $("#form").validate();
        });
    </script>
</body>
</html>
