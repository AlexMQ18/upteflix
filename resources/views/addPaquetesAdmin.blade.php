@extends('layout_admin.admin')

@section('titulo')
    <title>VISTA ADMIN | Paquetes</title>
@endsection

@section('css')
    <!-- Estilos custom -->
    <link rel="stylesheet" href="css/crudPeliculas_beta.css">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <!-- Fuentes -->
    <link href="https://fonts.googleapis.com/css2?family=Raleway:wght@100&display=swap" rel="stylesheet">
@endsection

@section('contenido')
    <div class="contenedor">
        <form class="contenedor__form"  action="{{route('add.paquete')}}" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            <h1 class="form__h1">Agregar un paquete.</h1>
            <label>Ingresa el nombre del paquete.</label>
            <input class="contenedor__input" type="text" name="nombrePaquete">
            <label>Ingresa el precio del paquete</label>
            <div class="contenedor__number">
                <label>$ </label>
                <input class="contenedor__input" type="number" min="0"  max="1000" name="precio">
            </div>
            <label>Ingresar descripcion del paquete</label>
            <textarea class="contenedor__textarea" id="" cols="30" rows="10" name="descripcion" placeholder="La descripcion debe tener como mucho 18 palabras."></textarea>
            <label>Ingresa la resolucion del paquete.</label>
            <select class="contenedor__select" name="resolucion">
                <option value="720">720p</option>
                <option value="1080">1080p</option>
                <option value="1440">1440p</option>
            </select>
            @if(isset($estatus))
                @if($estatus == "error")
                    <label class="label-error">{{$mensaje}}</label>
                @endif
                @if($estatus == "success")
                    <label class="label-success">{{$mensaje}}</label>
                @endif
            @endif
            <input class="contenedor__submit" type="submit">
        </form>
    </div>
@endsection

@section('js')

@endsection
