@extends('layout_admin.admin')

@section('titulo')
    <title>VISTA ADMIN | Peliculas</title>
@endsection

@section('css')
    <!-- Estilos custom -->
    <link rel="stylesheet" href="css/estilosPaquetesAdmin.css">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <!-- Fuentes -->
    <link href="https://fonts.googleapis.com/css2?family=Raleway:wght@100&display=swap" rel="stylesheet">
@endsection

@section('contenido')
        @php $paquete = \App\Models\Paquete::get();    @endphp
        <div class="contenedor">
        @foreach($paquete as $uso)
                <div id="ventana_emergente" class="contenedor__ventana-confirmar" style="display: none;">
                    <h1 class="ventana-emergente__h1-texto"></h1>
                    <button id="ventana-emergente__button-si">Sí</button>
                    <button id="ventana-emergente__button-no">Cancelar</button>
                </div>
                <div id="ventana_emergente-2" class="contenedor__ventana-confirmar" style="display: none;">
                    <h1 class="ventana-emergente__h1-texto"></h1>
                    <button id="ventana-emergente__button-no-2">Cancelar</button>
                </div>
                <div id="carta_paquete{{$uso->id_paquete}}" class="contenedor__paquete">
                    <h1>{{$uso->nombre_paquete}}</h1>
                    <div class="contenedor-icono">
                        @if($uso->resolucion == "720")
                            <span>HD</span>
                        @endif
                        @if($uso->resolucion == "1080")
                            <span>1080</span>
                        @endif
                        @if($uso->resolucion == "1440")
                            <span>2K</span>
                        @endif
                    </div>
                    <p><b>Descripcion:</b> {{$uso->descripcion}}</p>
                    <p><b>Precio: </b> ${{$uso->precio}}</p>
                    <p><b>Resolución:</b> {{$uso->resolucion}}p</p>
                    <div class="contenedor__buttons">
                        <a href="{{route('actualizar.paquete.admin',$uso->id_paquete)}}" class="button__actualizar">Actualizar</a>
                        <a id="eliminar_button{{$loop->index + 1}}" class="button__eliminar">Eliminar</a>
                    </div>
                </div>
        @endforeach
        </div>
@endsection

@section('js')
    <script src="js/jquery-3.6.0.js"></script>
    <script>
        $(document).ready(function (){
            let idPaquete = 0;

            @foreach($paquete as $uso)
            $("#eliminar_button{{$loop->index + 1}}").click(function (e){
                e.preventDefault();
                idPaquete = {{$uso->id_paquete}}
                console.log("click al boton: " + {{$loop->index + 1}});
                $('#ventana_emergente').css('display','block');
                $('#ventana_emergente h1').html('¿Estas seguro de eliminar el paquete: {{$uso->nombre_paquete}}?');

            });
            @endforeach

            $('#ventana-emergente__button-no').click(function (e){
                $('#ventana_emergente').css('display','none');
            });
            $('#ventana-emergente__button-si').click(function (e){
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: "POST",
                    url: "{{route('ventana.emergente.paquetes')}}",
                    data: {'id_paquete': idPaquete},
                    success: function(r){
                        console.log(r);
                        $('#ventana_emergente').css('display','none');
                        $('#ventana_emergente-2').css('display','block');
                        $('#ventana_emergente-2 h1').html('¡Paquete borrado exitosamente!');
                        $('#ventana-emergente__button-no-2').html('Cerrar');
                        $('#ventana-emergente__button-no-2').click(function (e){
                            $('#ventana_emergente-2').css('display','none');
                        });
                        $('#carta_paquete'+idPaquete).css('display','none');
                    }
                });
            });

        });
    </script>
@endsection

