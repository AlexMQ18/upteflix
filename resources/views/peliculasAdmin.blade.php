       @extends('layout_admin.admin')

       @section('titulo')
           <title>VISTA ADMIN | Peliculas</title>
       @endsection

       @section('css')
            <!-- Estilos custom -->
            <link rel="stylesheet" href="css/peliculasAdmin.css">
            <link rel="preconnect" href="https://fonts.gstatic.com">
            <!-- Fuentes -->
            <link href="https://fonts.googleapis.com/css2?family=Raleway:wght@100&display=swap" rel="stylesheet">
       @endsection

        @section('contenido')
        <div class="contenedor">
        @php $pelicula = \App\Models\Pelicula::get();    @endphp
        @foreach($pelicula as $uso)
            <div id="ventana_emergente" class="contenedor__ventana-confirmar" style="display: none;">
                <h1 class="ventana-emergente__h1-texto"></h1>
                <button id="ventana-emergente__button-si">Sí</button>
                <button id="ventana-emergente__button-no">Cancelar</button>
            </div>
            <div id="ventana_emergente-2" class="contenedor__ventana-confirmar" style="display: none;">
                    <h1 class="ventana-emergente__h1-texto"></h1>
                    <button id="ventana-emergente__button-no-2">Cancelar</button>
                </div>
            <div class="contenedor__pelicula" id="carta_pelicula{{$uso->id_pelicula}}">
                <h1 class="contenedor__titulo">{{$uso->nombre}}</h1>
                <img class="contenedor__img"src="{{$uso->ruta_imagen1}}">
                <p class="contenedor__categoria">Categoria: {{$uso->categoria}}</p>
                <p class="contenedor__duracion">Duración: {{$uso->tiempo_duracion}} min</p>
                <p class="contenedor__descripcion">Descripción: {{$uso->descripcion}}</p>
                <p class="contenedor__anio">Año: {{$uso->anio}}</p>
                <div class="contenedor__buttons">
                    <a  href="{{route('actualizar.peliculas.admin',$uso->id_pelicula)}}" class="button__actualizar">Actualizar</a>
                    <a id="eliminar_button{{$loop->index + 1}}" class="button__eliminar">Eliminar</a>
                </div>
            </div>
        @endforeach
        </div>
        @endsection

        @section('js')
        <script src="js/jquery-3.6.0.js"></script>
        <script>
            $(document).ready(function (){
                let idPelicula = 0;

                @foreach($pelicula as $uso)
                $("#eliminar_button{{$loop->index + 1}}").click(function (e){
                    e.preventDefault();
                    idPelicula = {{$uso->id_pelicula}}
                    console.log("click al boton: " + {{$loop->index + 1}});
                    $('#ventana_emergente').css('display','block');
                    $('#ventana_emergente h1').html('¿Estas seguro de eliminar la pelicula: {{$uso->nombre}}?');

                });
                @endforeach

                $('#ventana-emergente__button-no').click(function (e){
                    $('#ventana_emergente').css('display','none');
                });
                $('#ventana-emergente__button-si').click(function (e){
                    console.log("Id pelicula a eliminar: " + idPelicula);
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        type: "POST",
                        url: "{{route('ventana.emergente')}}",
                        data: {'id_pelicula': idPelicula},
                        success: function(r){
                            console.log(r);
                            $('#ventana_emergente').css('display','none');
                            $('#ventana_emergente-2').css('display','block');
                            $('#ventana_emergente-2 h1').html('¡Pelicula borrada exitosamente!');
                            $('#ventana-emergente__button-no-2').html('Cerrar');
                            $('#ventana-emergente__button-no-2').click(function (e){
                                $('#ventana_emergente-2').css('display','none');
                            });
                            $('#carta_pelicula'+idPelicula).css('display','none');
                        }
                    });
                });

            });
        </script>
        @endsection

