<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pelicula extends Model
{
    //identifica el nombre de la tabla.
    protected $table = "pelicula";
    //identifica la primary key de la tabla.
    protected $primaryKey = 'id_pelicula';
    

}
