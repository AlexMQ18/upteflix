<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Paquetes</title>
    <!-- Estilos custom -->
    <link rel="stylesheet" href="css/estilosRegistro.css">
    <link rel="stylesheet" href="css/estilosPaquetes.css">
    <link href="../css/estilosRegistroPago.css" rel="stylesheet"/>
    <style type="text/css">
        .container{
            margin-top: 70px;
        }
        .container h1{
            margin: 25px 0px;
            text-align: center;
        }
        .container h2{
            margin: 10px 0px;
        }
        #register_form fieldset:not(:first-of-type) {
            display: none;
        }
        .input-custom{
            margin: 5px 0px;
            font-size: 15px;
            border: 2px solid #000000;
            padding: 10px;
            display: block;
            width: 100%;
            border-radius: 4px;
            outline-color: #d00000;
        }
        .buttons-container{
            text-align: right;
        }
        .buttons-container input[type="button"]{
            margin: 10px 0px 5px 0px;
        }
        .last-buttons-container{
            text-align: right;
        }
    </style>
    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <!-- Fuentes -->
    <link href="https://fonts.googleapis.com/css2?family=Raleway:wght@100&display=swap" rel="stylesheet">
    <!-- Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body>
<header>
    <nav class="div-navbar-nav">
        <i class="div-navbar-nav-logo"><img src="https://dewey.tailorbrands.com/production/brand_version_mockup_image/938/5552771938_b0ed4e40-4af4-4d93-b5e5-278f289700c3.png?cb=1624927903" width="170px" height="45px"></i>
    </nav>
</header>
<div class="container">
    @php $paquetes = \App\Models\Paquete::get();    @endphp
    <h1>En solo 3 pasos disfrutaras de nuestros servicios.</h1>
    <div class="progress">
        <div class="progress-bar bg-danger" role="progressbar" aria-valuemin="0" aria-valuemax="100"></div>
    </div>
    <form id="register_form" method="post" action="{{route('registro.form')}}">
        {{csrf_field()}}
        <fieldset>
            <h2>Paso 1: Eleccion del paquete</h2>
            <div class="contenedor-paquetes">
                <input id="inputVariable" type="hidden" name="paquete_seleccionado" value="">
                @foreach($paquetes as $uso)
                <div class="contenedor__paquete">
                    <h1>{{$uso->nombre_paquete}}</h1>
                    <div class="contenedor-icono">
                        @if($uso->resolucion == "720")
                            <span>HD</span>
                        @endif
                        @if($uso->resolucion == "1080")
                            <span>1080</span>
                        @endif
                        @if($uso->resolucion == "1440")
                            <span>2K</span>
                        @endif
                    </div>
                    <p>{{$uso->descripcion}}</p>
                    <p class="p__precio"> ${{$uso->precio}}</p>
                    <a href="#" class="ancla_paquete" ">Seleccionar este paquete</a>
                    <input class="esc" type="hidden" value="{{$uso->nombre_paquete}}">
                </div>
                @endforeach
            </div>
            <div class="buttons-container">
                <input type="button" class="next-form btn btn-outline-danger buttons-custom-next" value="Siguiente" />
            </div>
        </fieldset>
        <fieldset class="info_contacto">
            <h2>Paso 2: Informacion de contacto.</h2>
            <div class="form-group">
                <label for="name">Nombre del perfil</label>
                <input type="text" class="input-custom" name="nombrePerfil" id="idNombrePerfil">
            </div>
            <div class="form-group">
                <label for="email">Correo</label>
                <input type="text" class="input-custom" required id="idCorreo" name="correo" @if(isset($correo)) value="{{$correo}}" @else placeholder="ejemplo@correo.com" @endif >
            </div>
            <div class="form-group">
                <label for="password">Contraseña</label>
                <input type="password" class="input-custom" name="contrasenia1" id="password1">
            </div>
            <div class="form-group">
                <label for="password">Repita su contraseña</label>
                <input type="password" class="input-custom" name="contrasenia2" id="password2">
            </div>
            <div class="buttons-container">
                <input type="button" name="previous" class="previous-form btn btn-secondary buttons-custom-previous" value="Anterior" />
                <input type="button" name="next" class="next-form btn btn-outline-danger buttons-custom-next" value="Siguiente" />
            </div>
        </fieldset>
        <fieldset>
            <h2> Paso 3: Informacion de pago</h2>
            <div class="container-checkout">
                <div class="col1">
                    <div class="card">
                        <div class="front">
                            <div class="type">
                                <img class="bankid"/>
                            </div>
                            <span class="chip"></span>
                            <span class="card_number">&#x25CF;&#x25CF;&#x25CF;&#x25CF; &#x25CF;&#x25CF;&#x25CF;&#x25CF; &#x25CF;&#x25CF;&#x25CF;&#x25CF; &#x25CF;&#x25CF;&#x25CF;&#x25CF; </span>
                            <div class="date"><span class="date_value">MM / YYYY</span></div>
                            <span class="fullname">CREDIT CARD</span>
                        </div>
                        <div class="back">
                            <div class="magnetic"></div>
                            <div class="bar"></div>
                            <span class="seccode">&#x25CF;&#x25CF;&#x25CF;</span>
                            <span class="chip"></span><span class="disclaimer"></span>
                        </div>
                    </div>
                </div>
                <div class="col2">
                    <label>Numero de tarjeta</label>
                    <input name="numeroTarjeta" class="input-custom" type="text" ng-model="ncard" maxlength="19" onkeypress='return event.charCode >= 48 && event.charCode <= 57'/>
                    <label>Nombre de la tarjeta</label>
                    <input name="nombreTarjeta" class="input-custom" type="text" placeholder=""/>
                    <label>Fecha de expiracion</label>
                    <input name="fechaExp" class="input-custom" type="date"
                           min="2021-11-01" max="2029-12-31"/>
                    <label>Numero de seguridad</label>
                    <input name="cvc" class="input-custom" type="text" placeholder="CVC" maxlength="3" onkeypress='return event.charCode >= 48 && event.charCode <= 57'/>
                </div>
            </div>
            <div class="last-buttons-container">
                <input type="button" name="previous" class="previous-form btn btn-secondary buttons-custom-previous" value="Anterior" />
                <input type="submit" name="submit" class="submit btn btn-danger buttons-custom-submit" value="Suscribirse" />
            </div>
        </fieldset>


    </form>
</div>

</body>
<script src="js/jquery-3.6.0.js"></script>
<script src="js/sliderScript.js"></script>
<script>
    $(document).ready(function (){

        let nombrePaquete;
        $(".ancla_paquete").click(function (){
            //elemento no seleccionado

            $('.contenedor__paquete').css('background','#fff');
            $('.contenedor__paquete').css('color','#000');
            $('.contenedor-icono').css('color','#000');
            $('.contenedor__paquete').css('border','2px solid #d00000');
            $('.contenedor__paquete').next('.ancla_paquete').css('color','#000');
            $('.ancla_paquete').html('Seleccionar este paquete');
            $('.ancla_paquete').css('color','#000');
            $('.ancla_paquete').css('font-size','15px');

            //elemento selccionado
            $(this).closest('.contenedor__paquete').css('background-color','#f00','color','#fff');
            $(this).closest('.contenedor__paquete').css('color','#fff');
            $(this).html('Seleccionado');
            $(this).css('font-size','18px');
            $(this).css('color','#fff');
                //sacar el nombre del paquete seleccionado

            nombrePaquete = $(this).next('.esc').val();
            $('#inputVariable').val(nombrePaquete);
            console.log(nombrePaquete);
        });

    });
</script>
</html>
