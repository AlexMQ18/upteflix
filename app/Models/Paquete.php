<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Paquete extends Model
{
    //identifica el nombre de la tabla.
    protected $table = "paquete";
    //identifica la primary key de la tabla.
    protected $primaryKey = 'id_paquete';
}
