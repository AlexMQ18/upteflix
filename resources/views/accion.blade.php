@extends('layaout usuario.principal')

@section('titulo')
    <title>ACCION</title>
@endsection

@section('css')
    <link rel="stylesheet" href="css/peliculas.css">
    <title> Inicio </title>
@endsection
@section('contenido')
    <div class="container">
        <h3 class=" pb-4 mb-4 pt-4 font-italic border-bottom">
            Las mejores peliculas de Accion:
        </h3>
        <div class="input-group pb-4">
            <input type="text" class="form-control mr-sm-2"type="search" placeholder="Ingrese pelicula">
            <div class="input-group-append">
                <button class="btn btn-danger">Buscar</button>
            </div>
        </div>
        @php $pelicula = \App\Models\Pelicula::where('categoria','Accion')->get(); @endphp
        <ul class="contenedor-lista">
            @foreach($pelicula as $uso)
            <li>
                <div class="contenedor-lista__pelicula">
                    <a href="#">
                        <img src="{{$uso->ruta_imagen1}}" class="pelicula__img">
                    </a>
                    <span class="pelicula__duracion">{{$uso->tiempo_duracion}} mins</span>
                    <a href="{{route('vista.usuario.video')}}" class="pelicula__boton-play">
                        <span class="botton-play__img"><img src="https://image.flaticon.com/icons/png/128/3039/3039386.png" width="50" height="50" alt=""></span>
                    </a>
                </div>
                <div class="contenedor-lista__nombre-pelicula">{{$uso->nombre}}</div>
            </li>
            @endforeach
        </ul>
    </div>

@endsection

@section('js')
    <script src="../recursos/js/jquery.min.js"></script>
    <script src="../recursos/js/bootstrap.js"></script>
@endsection
