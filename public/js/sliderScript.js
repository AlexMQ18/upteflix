$(document).ready(function(){
    var form_count = 1, previous_form, next_form, total_forms;
    total_forms = $("fieldset").length;
    $(".next-form").click(function(){
        previous_form = $(this).closest('fieldset');
        next_form = $(this).closest('fieldset').next();
        next_form.show();
        previous_form.hide();
        setProgressBarValue(++form_count);
    });
    $(".previous-form").click(function(){
        previous_form = $(this).closest('fieldset');
        next_form = $(this).closest('fieldset').prev();
        next_form.show();
        previous_form.hide();
        setProgressBarValue(--form_count);
    });
    setProgressBarValue(form_count);
    function setProgressBarValue(value){
        var percent = parseFloat(100 / total_forms) * value;
        percent = percent.toFixed();
        $(".progress-bar")
            .css("width",percent+"%")
            if(percent == 100){
                $(".progress-bar").html("¡Esta todo listo!")
            }else if(percent < 100){
                $(".progress-bar").html("")
            }
    }
// Handle form submit and validation
    $( "#register_form" ).submit(function(event) {
        var error_message = '';
        if(!$("#inputVariable").val()) {
            error_message+="Por favor seleccione un paquete";
        }
        if(!$("#idNombrePerfil").val()) {
            error_message+="Por favor ingrese un nombre para su perfil";
        }
        if(!$("#idCorreo").val()) {
            error_message+="<br>Por favor ingrese un correo";
        }
        if(!$("#password1").val()) {
            error_message+="<br>Por favor ingrese una contraseña";
        }
        if(!$("#password2").val()) {
            error_message+="<br>Por favor repita su contraseña";
        }
// Display error if any else submit form
        if(error_message) {
            $('.alert-success').removeClass('hide').html(error_message);
            return false;
        } else {
            return true;
        }
    });
});
