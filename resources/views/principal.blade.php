@extends('layaout usuario.principal')

@section('titulo')
    <title>PRINCIPAL</title>
@endsection

@section('css')
<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<title>Designlopers</title>
	<link rel="stylesheet" href="css/style.css">
        <title> Inicio </title>
@endsection
@section('contenido')
<body>
	<section class="banner">
		<div class="banner-content">
			<h1>Bienvenido a UPTFLIX</h1>	
		</div>
	</section>
	
	<section class="full-width formated-section">
		<h2 class="text-center font-oswald">PROXIMAMENTE</h2><br>
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
					<img src="https://irs.www.warnerbroslatino.com/keyart-jpeg/movies/media/browser/lm_sjam2_insta_vert_tsr_intl_1638x2048.jpg" alt="Sucursal" class="img-responsive img-rounded" width="300" height="500">
					<h3 class="text-center">Space</h3>
					<p class="text-center">
                    Una reliquia libera a una horda de extraterrestres sangrientos de sangre a bordo de una nave
					</p>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
					<img src="https://cl.buscafs.com/www.tomatazos.com/public/uploads/images/103322/103322.jpg" alt="Sucursal" class="img-responsive img-rounded" width="300" height="500">
					<h3 class="text-center">The Purgue</h3>
					<p class="text-center">
                    The Purge es una serie cinematográfica de terror estadounidense, centrada en una serie de películas de acción y suspenso
					</p>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
					<img src="https://activicities.com/wp-content/uploads/2020/12/Calendario-de-estrenos-cine-2021-Spiderman-3.jpg" alt="Sucursal" class="img-responsive img-rounded" width="300" height="500">
					<h3 class="text-center">Spider-Man</h3>
					<p class="text-center">
                    Spider-Man es un superhéroe ficticio creado por los escritores y editores Stan Lee y Steve Ditko. Apareció por primera vez en el cómic de antología Amazing
					</p>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
					<img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTxe-CvJHUcLE4F4kohPETXgrqbtBspJqjdgkfKNNFHIMcZx7ImNh_7BJ8XhaLYm1kYm6g&usqp=CAU" alt="Sucursal" class="img-responsive img-rounded" width="300" height="500">
					<h3 class="text-center">Black Window</h3>
					<p class="text-center">
					Black Widow es una próxima película de superhéroes estadounidense basada en el personaje de Marvel Comics del mismo nombre
					</p>
				</div>
			</div>
		</div>
	</section>
	<script src="js/jquery-3.1.0.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/main.js"></script>
</body>
@endsection

@section('js')

@endsection
