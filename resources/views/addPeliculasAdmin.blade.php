@extends('layout_admin.admin')

@section('titulo')
    <title>Vista administrador | add productos</title>
@endsection

@section('css')
    <!-- Estilos custom -->
    <link rel="stylesheet" href="css/crudPeliculas_beta.css">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <!-- Fuentes -->
    <link href="https://fonts.googleapis.com/css2?family=Raleway:wght@100&display=swap" rel="stylesheet">
@endsection

@section('contenido')
    <div class="contenedor">
    <form class="contenedor__form"  action="{{route('add.peliculas')}}" method="post" enctype="multipart/form-data">
        {{csrf_field()}}
        <h1 class="form__h1">Registrar PELICULAS.</h1>
        <label>Ingresar el nombre de la pelicula.</label>
        <input class="contenedor__input" type="text" name="nombre">
        <label>Ingresar categoria de la pelicula</label>
        <input class="contenedor__input" type="text" name="categoria">
        <label>Ingresar duracion de la pelicula</label>
        <div class="contenedor__number">
            <label>Mins: </label>
            <input class="contenedor__input" type="number" min="0"  max="300" name="minutos">
        </div>
        <label>Ingresar descripcion de la pelicula</label>
        <textarea class="contenedor__textarea" id="" cols="30" rows="10" name="descripcion"></textarea>
        <label>Ingresar imagen de portada</label>
        @error('img1')
        <label class="label-error">{{$message}}</label>
        @enderror
        <input type="file" class="contenedor__file" name="img1" accept="image/*">
        <label>Ingresar segunda imagen secundaria</label>
        @error('img2')
        <label class="label-error">{{$message}}</label>
        @enderror
        <input type="file" class="contenedor__file" name="img2" accept="image/*">
        <label>Ingresar año de estreno</label>
        <input type="date" class="contenedor__date"
               value="2018-07-22"
               min="1950-01-01" max="2021-07-31" name="anio">
        @if(isset($estatus))
            @if($estatus == "error")
                <label class="label-error">{{$mensaje}}</label>
            @endif
                @if($estatus == "success")
                    <label class="label-success">{{$mensaje}}</label>
                @endif
        @endif
        <input class="contenedor__submit" type="submit">
    </form>
</div>
@endsection
@section('js')

@endsection
