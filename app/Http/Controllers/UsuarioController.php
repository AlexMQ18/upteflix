<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Usuario;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class UsuarioController extends Controller
{
    public function login(){
        return view("login");
    }
    public function principal(){
        return view("principal");
    }
    public function pago($idPaquete){
        return view("pago");
    }
    public function admin(){
        return view("admin");
    }
    //vista provisional para el registro del usuario.
    public function registro(){
        return view("vistaRegistro");
    }
    public function paquetes(){
        return view("paquetes");
    }
    public function verificaCorreo(Request $datosForm){

        //verificar si el correo existe en la bd o no.
        $correo = $datosForm->input_correo;
        $correo_encontrado = Usuario::where('correo',$correo)->first();
        if ($correo_encontrado){
            return view("inicio",["estatus"=>"error", "mensaje" => "¡El correo ya esta registrado! entra en Iniciar Sesion"]);
        }else{
            return view("vistaRegistro", ["correo" => $correo]);
        }
   }
   public function registroSlider(Request $datos){
        //validaciones pendientes
       echo "hecho";

   }
   public function verificaCredencialesLogin(Request $datos){

        $correo = $datos->correo;
        $contrasenia = $datos->contrasenia;

       if(!$correo || !$contrasenia)
           return view("login",["estatus"=> "error", "mensaje"=> "¡Completa los campos!"]);

       $usuario = Usuario::where("correo",$datos->correo)->first();
       if(!$usuario)
           return view("login",["estatus"=> "error", "mensaje"=> "¡El correo no esta registrado!"]);

       if(!Hash::check($datos->contrasenia,$usuario->contrasenia))
           return view("login",["estatus"=> "error", "mensaje"=> "¡Datos incorrectos!"]);

       //return view(""); retornara a la pagina principal.

       //variable de sesion
       //Session::put('usuario',$usuario);

   }


}
